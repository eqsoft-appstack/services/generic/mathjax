ARG BASE_IMAGE=
ARG BASE_TAG=
ARG HTTP_PROXY=
ARG HTTPS_PROXY=
ARG http_proxy=
ARG https_proxy=

FROM ${BASE_IMAGE}:${BASE_TAG}

LABEL maintainer="Stefan Schneider <eqsoft4@gmail.com>"

USER root

ENV DEBIAN_FRONTEND noninteractive
ENV TZ=Europe/Berlin
SHELL ["/bin/bash", "-c"]

RUN <<EOF
set -e
apt-get update
apt-get install -y --no-install-recommends \
tzdata \
apt-transport-https \
ca-certificates \
software-properties-common \
curl \
gnupg \
nano
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime
echo $TZ > /etc/timezone
curl -sL https://deb.nodesource.com/setup_current.x | bash -
apt-get update
apt-get -y install nodejs
EOF

ARG MATHJAX_PORT=8013
ENV MATHJAX_PORT=$MATHJAX_PORT

# mathjax
RUN <<EOF
set -e
mkdir -p /opt/mathjax
cd /opt/mathjax
npm install https://github.com/mathjax/MathJax-node/tarball/master
npm install https://github.com/tiarno/mathjax-server/tarball/master
cd node_modules
ln -s mathjax-node MathJax-node
EOF

COPY ./files/ /opt/mathjax/

CMD ["/usr/bin/node", "/opt/mathjax/mathjax.js"]
