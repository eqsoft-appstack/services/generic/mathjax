#!/bin/sh

ret=$(docker run -d --rm --name $test_container_name $test_image)

echo $ret

sleep 2

ret=$(docker logs $test_container_name 2>&1 | grep "Server listening on port 8013")

if [ "$?" != "0" ] || [ -z "$ret" ] ; then
  log 3 "testing image not successfull!"
  exit 1
fi

log 7 "testing image successfull $ret"
